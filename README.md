# Proving Ground Summer 2016 SDK #

![ProvingGroundReadMes.jpg](https://bitbucket.org/repo/LpEap8/images/1760167380-ProvingGroundReadMes.jpg)

Computational tools and data-driven technologies are transforming contemporary design practice. As believers in a data-driven process, we have assembled a collection of workflows, algorithms, and samples to highlight the potential for leveraging data for a better built environment. We’ve borrowed the term “Software Development Kit” (SDK) to signal the growing connection between the tools we make and what we make with our tools.

This summer, we initiated a process of developing prototypes and sample datasets using computational technologies and workflows. Each dataset is a starting point for continued development and consideration. By making these tools open source, we hope the digital design community can pick up on the threads we have started here. Each folder contains the project files along with a ReadMe to help you get started. Each SDK dataset may require a different mix of third-party software and plugins to run.

We hope you enjoy them!

### About Proving Ground ###

Proving Ground provides creative services for a data-driven building industry. You can learn more about us and our work at [ProvingGround.io](http://provingground.io).

* [Strategy](https://provingground.io/services/strategy/)
* [Customization](https://provingground.io/services/projects/custom-tools-automation/)
* [Projects](https://provingground.io/services/projects/)

### About the Proving Ground 2016 Summer Team ###
The Summer 2016 SDK was authored as part of an 8-week internship. The summer team kicked things off by participating in workshops focused on computational design technologies and new workflows. The team then defined a series of design problems to guide the development of the prototypes which are captured within this dataset.  

You can reach the 2016 Summer Team team through LinkedIn…
 
* [Elizabeth Heldridge](https://www.linkedin.com/in/elizabeth-heldridge-a28a60b8) 
* [Kristen Schulte](https://www.linkedin.com/in/kristenschulte ) 
* [Charles Weak](https://www.linkedin.com/in/charles-weak-0b596357) 

### License ###

Copyright © 2016 Proving Ground LLC

Permission is hereby granted, free of charge, to any person obtaining a copy of the Summer 2016 SDK files and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.